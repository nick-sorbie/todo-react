import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { shallow, mount, render } from 'enzyme';

test('Submit button is disabled on load', () => {
  const wrapper = mount(<App/>);
  expect(wrapper.find('.App-submit').prop('disabled')).toBe(true);
});

test('Submit button is disabled until at least the title is populated', () => {
  const wrapper = mount(<App/>);
  const text = 'Walk the dog';
  wrapper.setState({'inputTitle' : text});
  expect(wrapper.find('.App-submit').prop('disabled')).toBe(false);
  expect(wrapper.find('#title').props().value).toBe(text);
});

test('Submit button is disabled until at least the description is populated', () => {
  const wrapper = mount(<App/>);
  const text = 'Take out rover before it rains';
  wrapper.setState({'inputDescription' : text});
  expect(wrapper.find('.App-submit').prop('disabled')).toBe(false);
  expect(wrapper.find('#description').props().value).toBe(text);
});

test('Clicking submit adds a new todo', () => {
  const wrapper = mount(<App/>);
  const todos = wrapper.state('currentTodos');
  const title = 'Walk the dog', description = 'Take out rover before it rains';
  wrapper.setState({
    'inputDescription' : title,
    'inputTitle' : description
  });
  wrapper.find('.App-submit').simulate('click');
  expect(wrapper.state('currentTodos')).not.toBe([]);
});

test('Clicking delete removes the todo', () => {
  const wrapper = mount(<App/>);
  const todos = wrapper.state('currentTodos');
  const title = 'Walk the dog', description = 'Take out rover before it rains';
  wrapper.setState({
    'inputDescription' : title,
    'inputTitle' : description
  });
  wrapper.find('.App-submit').simulate('click');
  wrapper.find('#App-remove_0').simulate('click');
  
  var actual = JSON.stringify(wrapper.state('currentTodos'));
  var expected = JSON.stringify([]);
  expect(actual).toBe(expected);
});

test('Clicking expand icon opens a modal', () => {
  const wrapper = mount(<App/>);
  const todos = wrapper.state('currentTodos');
  const title = 'Walk the dog', description = 'Take out rover before it rains';
  wrapper.setState({
    'inputDescription' : title,
    'inputTitle' : description
  });
  wrapper.find('.App-submit').simulate('click');
  expect(wrapper.find('.ReactModal__Content').exists()).toBe(false);
  wrapper.find('#App-expand_0').simulate('click');
  expect(wrapper.find('.ReactModal__Content').exists()).toBe(true);
});
