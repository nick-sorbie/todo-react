import React, { Component } from 'react';
import Modal from 'react-modal';
import {customStyles} from './modalStyles';
import PropTypes from 'prop-types';


class ModalBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
          modalShown: props.modalShown,
        };
      }

    componentWillMount() {
        Modal.setAppElement('body');
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ modalShown: nextProps.isOpen });  
      }

    render() {
    const {closeModal, modalTitle, modalDescription} = this.props;
      return (
        <Modal
        isOpen={this.state.modalShown}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Modal">
        <h1 style={customStyles.h1}>{modalTitle}</h1>
        <p style={customStyles.p}>{modalDescription}</p>
        <i style={customStyles.close} className="fa fa-times-circle" onClick={closeModal} aria-hidden="true"></i>
      </Modal>
    );
    }
  }
  ModalBox.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    closeModal: PropTypes.func.isRequired,
    modalTitle: PropTypes.string,
    modalDescription: PropTypes.string,
  };

export default ModalBox;