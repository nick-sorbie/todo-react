export const customStyles = {
    overlay : {
      position          : 'fixed',
      top               : 0,
      left              : 0,
      right             : 0,
      bottom            : 0,
      backgroundColor   : 'rgba(0, 0, 0, 0.5)'
    },
    content : {
        position                   : 'absolute',
        top                        : '200px',
        left                       : '200px',
        right                      : '200px',
        bottom                     : '200px',
        background                 : '#3b487b',
        overflow                   : 'auto',
        WebkitOverflowScrolling    : 'touch',
        borderRadius               : '10px',
        outline                    : 'none',
        padding                    : '50px',
        color                      : '#fff',
        textAlign                  : 'center',
    },
    h1 : {
        fontFamily                 : 'alwaysforever',
        fontSize                   : 50,
    },
    p : {
        fontSize                   : 20,
        fontFamily                 : "sans-serif",
    },
    close : {
        fontSize                   : 40,
        position                   : "absolute",
        top                        : 20,
        right                      : 20,
        color                      : "#cec75e",
        cursor                     : "pointer",
    }
  }