import React, { Component } from 'react';
import ModalBox from './modal/modal.js';
import './css/app.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputTitle: '',
      inputDescription: '',
      currentTodos: [],
      modalShown: false,
      modalTitle: '',
      modalDescrption: '',
    };
    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.isEnabled = this.isEnabled.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  componentWillMount() {
    const currentTodos = localStorage.getItem('currentTodos')
    const ExistingTodos = currentTodos ? JSON.parse(currentTodos) : [];
    this.setState({
      currentTodos: ExistingTodos
    });
  }

  handleTextChange = (event) => {
    this.setState({[event.target.name] : event.target.value});
  }

  handleSubmit = () => {
    if (!this.isEnabled()) {
      return;
    }
    const currentTodos = this.state.currentTodos;
    currentTodos.push({
      "title" : this.state.inputTitle,
      "description" : this.state.inputDescription
    });

    localStorage.setItem('currentTodos', JSON.stringify(currentTodos));
    this.setState({
      currentTodos,
      inputTitle: '',
      inputDescription: ''
    });
  }

  deleteItem = (index) => {
    const currentTodos = this.state.currentTodos;
    currentTodos.splice( index, 1 )
    localStorage.setItem('currentTodos', JSON.stringify(currentTodos));
    this.setState({
      currentTodos
    });
  }

  viewItem = (index) => {
    const selectedTodo = this.state.currentTodos[index];
    this.setState({
      'modalShown':true,
      'modalTitle': selectedTodo.title,
      'modalDescription': selectedTodo.description
    });
  }

  isEnabled() {
    return (this.state.inputTitle.length > 0 || this.state.inputDescription.length > 0);
  }

  trimText(text){
    return text.length > 30 ? text.substring(0, 27) + "..." : text;
  }

  closeModal() {
    this.setState({'modalShown' : false});
  }

  render() {
    const currentTodos = this.state.currentTodos,
    disabled = this.isEnabled()  ? '' : 'disabled';
    return (
      <div className="App" aria-hidden={true}>
        <ModalBox
        isOpen={this.state.modalShown}
        closeModal={this.closeModal}
        modalTitle={this.state.modalTitle}
        modalDescription={this.state.modalDescription}
        />
        <header className="App-header">
          <h1 className="App-title">Things to do:</h1>
        </header>
        <div className="App-currentList">
          <ul>
            {currentTodos.map((value, index) => {
              const removeItem = () => this.deleteItem(index);
              const expandItem = () => this.viewItem(index);
              return (
                <li key={index}>
                  <i id={`App-expand_${index}`}  className="App-expand fa fa-plus-square-o" onClick={expandItem} aria-hidden="true"></i>
                  {value.title !== '' ? value.title : this.trimText(value.description)}
                  <i id={`App-remove_${index}`} className="App-remove fa fa-trash-o" onClick={removeItem} aria-hidden="true"></i>
                </li>)
              })}
          </ul>
        </div>
        <div className="App-input-section">
          <input
            id="title"
            className="App-input"
            value={this.state.inputTitle}
            onChange={this.handleTextChange}
            placeholder="New todo"
            maxLength="30"
            name="inputTitle"
          />
          <textarea
            id="description"
            value={this.state.inputDescription}
            className="App-input-description"
            onChange={this.handleTextChange}
            placeholder="Description" 
            rows="5"
            name="inputDescription"></textarea>
        
          <button className={`App-submit ${disabled}`} onClick={this.handleSubmit} disabled={!this.isEnabled()}>
            <i className="fa fa-plus" aria-hidden="true"></i>
          </button>
        </div>
      </div>
    );
  }
}

export default App;
