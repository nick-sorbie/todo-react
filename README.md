# Todo App

## explanation

Attempted technical test, with some added UI so that is doesn't look terrible. Not that reponsive.
Auto save create and edits of notes I would add given more time, also would move some more bits into individual components.

## Getting started
```
git clone https://gitlab.com/nick-sorbie/todo-react.git
```
```
cd todo-react
```
```
npm -i 
```
or 
```
yarn install
```
Then
```
num run start
```
or
```
yarn start
```
The navigate to http://localhost:3000/ if it doesn't auto load.

## Testing
```
num run test
```
or
```
yarn test
```